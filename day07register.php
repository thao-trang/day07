<!DOCTYPE html> 

<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Login</title>
<body>

    <fieldset style='width: 500px; height: 600; border:#ADD8E6 solid'>
        <?php
        session_start();
        date_default_timezone_set('Asia/Ho_Chi_Minh');

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $check = true;
                if(empty(inputHandling($_POST["name"]))){
                    echo "<div style='color: red;'>Hãy nhập tên</div>";
                    $check=false;
                }
                if (empty($_POST["gender"])) {
                    echo "<div style='color: red;'>Hãy chọn giới tính</div>";
                    $check=false;
                }
                if(empty(inputHandling($_POST["industryCode"]))){
                    echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
                    $check=false;
                }
                $birthOfDate = inputHandling($_POST["birthOfDate"]);
                if(empty($birthOfDate)){
                    echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
                    $check=false;
                }
                elseif (!validateDate($birthOfDate)) {
                    echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
                    $check=false;
                }
                if (!isset($_FILES["fileupload"])) {
                    echo "Dữ liệu không đúng cấu trúc";
                }
                if ($_FILES["fileupload"]['error'] != 0){
                  echo "Dữ liệu upload bị lỗi";
                }

                if($check){
                    $target_dir    = "uploads/";
                    $new_name = "uploads/".$_FILES["fileupload"]["name"]."_".date("YmdHis").".";
                    //Vị trí file lưu tạm trong server (file sẽ lưu trong uploads, với tên giống tên ban đầu)
                    $target_file   = $target_dir . basename($_FILES["fileupload"]["name"]);
    
                    $allowUpload   = true;
    
                    //Lấy phần mở rộng của file (jpg, png, ...)
                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    
                    // Cỡ lớn nhất được upload (bytes)
                    $maxfilesize   = 800000; 
    
                    ////Những loại file được phép upload
                    $allowtypes    = array('jpg', 'png', 'jpeg', 'gif');
    
                    $check = getimagesize($_FILES["fileupload"]["tmp_name"]);
                    if($check == false) 
                    {
                        echo "<div style='color: red;'>Không phải file ảnh</div>";
                        $allowUpload = false;
                    } 
                      // Kiểm tra nếu file đã tồn tại thì không cho phép ghi đè
                    // Bạn có thể phát triển code để lưu thành một tên file khác
                    if (file_exists($target_file)) 
                    {
                        echo "<div style='color: red;'>Tên file đã tồn tại trên server, không được ghi đè</div>";
                        $allowUpload = false;
                    }
                    // Kiểm tra kích thước file upload cho vượt quá giới hạn cho phép
                    if ($_FILES["fileupload"]["size"] > $maxfilesize)
                    {
                        echo "<div style='color: red;'>Không được upload ảnh lớn hơn $maxfilesize (bytes).</div>";
                        $allowUpload = false;
                    }
                      // Kiểm tra kiểu file
                    if (!in_array($imageFileType,$allowtypes ))
                    {
                        echo "<div style='color: red;'>Chỉ được upload các định dạng JPG, PNG, JPEG, GIF</div>";
                        $allowUpload = false;
                    }

                    $fileExt = explode('.', $_FILES["fileupload"]["name"]);
                    
                    $fileActualExt = strtolower(array_pop($fileExt));
                    
                    $fileNameNew = date('YmdHis');
                    $new_name =  $target_dir.str_replace(' ', '', join('.', $fileExt)) . "_" . $fileNameNew . "." . $fileActualExt;

                    echo $new_name;
                    if ($allowUpload==true && $check==true) 
                    {
                        // Xử lý di chuyển file tạm ra thư mục cần lưu trữ, dùng hàm move_uploaded_file
                        if (move_uploaded_file( $_FILES["fileupload"]["tmp_name"],$new_name))
                        {
                            // rename($_FILES["fileupload"]["name"],$new_name);
                            // echo "File ". basename( $_FILES["fileupload"]["name"])." Đã upload thành công.";
                            // echo $new_name;
                  
                            // echo "File lưu tại " . $target_file;
                  
                        }
                        else
                        {
                            echo "<div style='color: red;'>Có lỗi xảy ra khi upload file.</div>";
                            $allowUpload= false;
                        }
                    } 
                }
                
                if($allowUpload==true && $check==true){
                    $_SESSION = $_POST;
                    $_SESSION["filename"] = $new_name;
                    
                    header("location: listStudent.php");
                }
            }

            function inputHandling($data) {
                $data = trim($data);
                $data = stripslashes($data);
                return $data;
            }

            function validateDate($date){
                if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                    return true;
                } else {
                    return false;
                }

            }
        ?>
        <form style='margin: 20px 50px 0 35px' method="post" enctype="multipart/form-data">
            <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
                <tr height = '40px'>
                    <td style = 'background-color: #1e8dcc; 
                    vertical-align: top; text-align: left; padding: 5px 5px'>
                        <label style='color: white;'>Họ và tên</label>
                        <span style='color: red;'>*</span>
                    </td>
                    <td >
                        <input type='text' name = "name" style = 'line-height: 32px; border-color:#ADD8E6'>
                    </td>
                </tr>
                <tr height = '40px'>
                    <td style = 'background-color: #1e8dcc; vertical-align: top; text-align: left; padding: 5px 5px'>
                        <label style='color: white;'>Giới tính</label>
                        <span style='color: red;'>*</span>
                    </td>
                    <td >
                        <?php
                            $genderArr=array("Nam","Nữ");
                            for($x = 0; $x < count($genderArr); $x++){
                                echo"
                                    <label class='container'>
                                        <input type='radio' value=".$genderArr[$x]." name='gender'>"
                                        .$genderArr[$x]. 
                                    "</label>";
                            }
                        ?>  
                    </td>
                </tr>
                <tr height = '40px'>
                    <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: left; padding: 5px 5px'>
                        <label style='color: white;'>Phân Khoa</label>
                        <span style='color: red;'>*</span>
                    </td>
                    <td height = '40px'>
                        <select name='industryCode' style = 'border-color:#ADD8E6;height: 100%;width: 80%;'>
                            <?php
                                $industryCodeArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                                foreach($industryCodeArr as $x=>$x_value){
                                    echo"<option>".$x_value."</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr height = '40px'>
                    <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: left; padding: 5px 5px'>
                        <label style='color: white;'>Ngày sinh</label>
                        <span style='color: red;'>*</span>
                    </td>
                    <td height = '40px'>
                        <input type='date' name="birthOfDate" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#ADD8E6'>
                    </td>
                </tr>

                <tr height = '40px'>
                    <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: left; padding: 5px 5px'>
                        <label style='color: white;'>Địa chỉ</label>
                    </td>
                    <td height = '40px'>
                        <input type='text' name="address" style = 'line-height: 32px; border-color:#ADD8E6'> 
                    </td>
                </tr>

                <tr height = '40px'>
                    <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: left; padding: 5px 5px'>
                        <label style='color: white;'>Hình ảnh</label>
                    </td>
                    <td height = '40px'>
                        <input type="file" name="fileupload" id="myFile">
                    </td>
                </tr>

            </table>
            <button style='background-color: #49be25; border-radius: 10px; 
            width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
        </form>

    </fieldset>
</body>
</html>

